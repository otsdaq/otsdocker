cd /otsdaq/spack/

echo "Sourcing Spack environment setup"
. /otsdaq/spack/setup-env.sh
spack find 
spack compilers
spack compiler remove gcc@11.4.1
#rm -rf /otsdaq/spack/spack/opt/spack/linux-almalinux9-x86_64_v2/gcc-11.4.1/llvm-17.0.6-322krghbzbkam5ck2nyup46guivmhhvk
spack clean -a
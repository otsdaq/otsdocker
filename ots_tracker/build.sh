export KDIR=`ls -d -1 "/usr/src/kernels/"*/`

echo "OTSDAQ_CMS_BURNINBOX_SPACK_REF" >> ci_args.txt
echo $OTSDAQ_CMS_BURNINBOX_SPACK_REF >> ci_args.txt

echo "OTSDAQ_CMS_TRACKER_SPACK_REF" >> ci_args.txt
echo $OTSDAQ_CMS_TRACKER_SPACK_REF >> ci_args.txt


###################################################
#Installing Tracker and Burninbox packages
###################################################
cd /otsdaq
curl https://ipbus.web.cern.ch/doc/user/html/_downloads/fedora34-updates.repo -o /etc/yum.repos.d/fedora34-updates.repo
dnf -y install https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/34/Everything/x86_64/os/Packages/f/fedora-gpg-keys-34-1.noarch.rpm
curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.el9.repo -o /etc/yum.repos.d/ipbus-sw.repo
dnf clean all
dnf groupinstall -y uhal controlhub
ls /opt
export CACTUSROOT=/opt/cactus



wget https://github.com/protocolbuffers/protobuf/releases/download/v3.19.4/protobuf-all-3.19.4.tar.gz
tar zxvf protobuf-all-3.19.4.tar.gz
cd protobuf-3.19.4
./configure
make -j$(nproc) # $(nproc) ensures it uses all cores for compilation
make check
make install
ldconfig # refresh shared library cache.
pip3 install protobuf==3.19.4 # to use it from python
make clean


cd /otsdaq/spack
source setup-env.sh
cd ..

spack env activate ots

cd spack/repos
git clone https://gitlab.cern.ch/otsdaq/spack/otsdaq-cms-burninbox-spack.git && spack repo add otsdaq-cms-burninbox-spack
cd otsdaq-cms-burninbox-spack
git checkout $OTSDAQ_CMS_BURNINBOX_SPACK_REF
cd ..
git clone https://gitlab.cern.ch/otsdaq/spack/otsdaq-cms-tracker-spack.git && spack repo add otsdaq-cms-tracker-spack
cd otsdaq-cms-tracker-spack
git checkout $OTSDAQ_CMS_TRACKER_SPACK_REF

cd ..
spack add otsdaq-cmstracker@$SPACK_VERSION
spack add otsdaq-cmsburninbox@$SPACK_VERSION
spack concretize -f
spack install --no-checksum -j7
spack gc -y

###################################################
#Removing compiler
###################################################
cd /otsdaq/spack/
echo "Sourcing Spack environment setup"
. /otsdaq/spack/setup-env.sh
spack find 
spack compilers
spack compiler remove gcc@11.4.1
rm -rf /otsdaq/spack/spack/opt/spack/linux-almalinux9-x86_64_v2/gcc-11.4.1/llvm-17.0.6-322krghbzbkam5ck2nyup46guivmhhvk
spack clean -a